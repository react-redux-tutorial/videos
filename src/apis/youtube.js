import axios from 'axios';

const KEY = 'AIzaSyA-k7PLsovdARknapSlLhDsz8Qdi18Kc_s';

export default axios.create({
  baseURL: 'https://www.googleapis.com/youtube/v3',
  params: {
    part: 'snippet',
    maxResults: 5,
    type: 'video',
    key: KEY
  }
});